// android_sensorData
// Eric Pavey - 2010-10-10
// http://www.akeric.com
//
// Query the phone's accelerometer and magnetic field data, display on screen.
// Made with Android 2.1, Processing 1.5.1

//getOrientation
//by Cdriko Doutriaux
//from http://blogah.arvyoo.com/2011/02/android-obtenir-les-valeurs-dinclinaisons-du-smartphone/

//-----------------------------------------------------------------------------------------
// Imports required for sensor usage:
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorManager;
import android.hardware.SensorEventListener;

//-----------------------------------------------------------------------------------------
// Screen Data:
float sw, sh;
// Font Data:

PFont androidFont;

// capteurs
MySensorEventListener accSensorEventListener;
MySensorEventListener magSensorEventListener;
SensorManager _sensorManager;
Sensor _accelerometerSensor;
Sensor _magneticSensor;
float[] _accelerometerValues   = null;  //Valeur de l'accéléromètre
float[] _magneticValues          = null;  //Valeurs du champ magnétique
float _azimuth= 0;     //Valeur courante de la boussole
float _pitch= 0;    //Inclinaison haut - bas
float _roll= 0;    //Inclinaison droite - gauche
long timestamp;//memo de l'horloge utilise pour l'acceleration
float NS2S = 1.0f / 1000000000.0f;//convertisseur nanoSec/sec

float R[] = new float[9]; //Notre matrice de rotation

float controle=0;//pur debug
///infos 3D
PVector gravity=new PVector(0, 0, 0);
PVector linear_acceleration=new PVector(0, 0, 0);
PVector vitesse=new PVector(0, 0, 0);
PVector deplacement=new PVector(0, 0, 0);
PVector position=new PVector(0, 0, 0);
PVector[] chemin=new PVector[2];
//-----------------------------------------------------------------------------------------







//-----------------------------------------------------------------------------------------
// Override the parent (super) Activity class:
// States onCreate(), onStart(), and onStop() aren't called by the sketch.  Processing is entered at
// the 'onResume()' state, and exits at the 'onPause()' state, so just override them:

void onResume() {
  super.onResume();
  println("RESUMED! (Sketch Entered...)");
  // Build our Sensorevent Listener:

  magSensorEventListener = new MySensorEventListener();
  accSensorEventListener = new MySensorEventListener();


  //Récupération du gestionnaire de capteurs
  _sensorManager = (SensorManager)getSystemService(Context.SENSOR_SERVICE);
  //Récupération des capteurs
  _accelerometerSensor = _sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
  // _accelerometerSensor = _sensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION);
  println(_sensorManager.getSensorList(Sensor.TYPE_ALL));



  // -----------------------------------------------------------------liste des capteurs disponibles
  List<Sensor> msensorList = _sensorManager.getSensorList(Sensor.TYPE_ALL);    
  println(msensorList.size()+" capteurs ");
  for (int i=0;i<msensorList.size();i++) {
    int t=msensorList.get(i).getType();
    String type="..";
    switch(t) {
    case Sensor.TYPE_ACCELEROMETER:
      type="Sensor.TYPE_ACCELEROMETER";
      break;
    case Sensor.TYPE_ORIENTATION:
      type="Sensor.TYPE_ORIENTATION";
      break;
    case Sensor.TYPE_TEMPERATURE:
      type="Sensor.TYPE_TEMPERATURE";
      break;
    case Sensor.TYPE_PROXIMITY:
      type="Sensor.TYPE_PROXIMITY";
      break;
    case Sensor.TYPE_MAGNETIC_FIELD:
      type="Sensor.TYPE_MAGNETIC_FIELD";
      break;
    }   
    println("capteur "+i+":"+t+" "+type);
  }    
  //--------fin liste des capteurs-------------------------------------------------------------------------------------

  _magneticSensor = _sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
  //  reglage de la vitesse d'acquisition

  _sensorManager.registerListener(accSensorEventListener, _accelerometerSensor, SensorManager.SENSOR_DELAY_GAME);
  _sensorManager.registerListener(magSensorEventListener, _magneticSensor, SensorManager.SENSOR_DELAY_GAME);
}

void onPause() {
  // Unregister all of our SensorEventListeners upon exit:
  _sensorManager.unregisterListener(accSensorEventListener);
  _sensorManager.unregisterListener(magSensorEventListener);
  println("PAUSED! (Sketch Exited...)");
  super.onPause();
} 

//-----------------------------------------------------------------------------------------



