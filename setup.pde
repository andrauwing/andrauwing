int calibration=0;//calibration en cours
PVector correction=new PVector(0.,0.,0.);//vecteur de correction de l'acceleration
int curentStep=1;

boolean dessine=false;


public String sketchRenderer() {
  return A3D;
}


void setup() {
  
 calibration=0;
//timestamp=millis();
  

  chemin[0]=new PVector(0, 0, 0);
  chemin[1]=new PVector(0, 0, 0);

  orientation(PORTRAIT);
  //size(screenWidth, screenHeight, A2D);
  sw = screenWidth;
  sh = screenHeight;
  // Set this so the sketch won't reset as the phone is rotated:

  // Setup Fonts:
  String[] fontList = PFont.list();
  androidFont = createFont(fontList[0], 14, true);
  //androidFont=loadFont("font.vlw");
  textFont(androidFont);
  background(255);
  smooth();
  // stroke(0, 0, 0, 120);
  strokeWeight(2);
  fill(10);
}
