///interaction avec les boutons
void keyPressed() {
  if (key == CODED) {
    if (keyCode == BACK) {
      // do something here for the back button behavior

      // you'll need to set keyCode to 0 if you want to prevent quitting (see above)
    } 
    else if (keyCode == MENU) {
      // recentre le trace
      background(200);

      ///remise a zero du chemin
      curentStep=1;
      position.set(1.0, 1.0, 1.0);
      vitesse.set(1.0, 1.0, 1.0);
    }
  }
}

 public boolean surfaceTouchEvent(MotionEvent event) {
   ///calibration
     if (event.getAction() == MotionEvent.ACTION_DOWN) { 
   dessine=true;
    } else if (event.getAction() == MotionEvent.ACTION_UP) { 
   dessine=false;
    }
   
    // your code here
 
    // if you want the variables for motionX/motionY, mouseX/mouseY etc.
    // to work properly, you'll need to call super.surfaceTouchEvent().
    return super.surfaceTouchEvent(event);
  }
