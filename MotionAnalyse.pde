
// Setup our SensorEventListener
class MySensorEventListener implements SensorEventListener {
  void onSensorChanged(SensorEvent event) {
    //Récupération des valeurs
    // if (event.sensor.getType() == Sensor.TYPE_LINEAR_ACCELERATION) 
    if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) _accelerometerValues = event.values.clone();
    if (event.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD) _magneticValues = event.values.clone();

    if (_accelerometerValues != null && _magneticValues != null)//si on a les deux valeurs
    {
      float dT=0;
      if (timestamp != 0) {
        dT = (event.timestamp - timestamp) * NS2S;
      }
      timestamp = event.timestamp;
      //filtrage de l'accelerometre pour recuperer le gravite
      //determination de l'acceleration lineaire par soustraction de la gravite
      // alpha is calculated as t / (t + dT)
      // with t, the low-pass filter's time-constant
      // and dT, the event delivery rate
      //
      float t=0.2;
      float alphat = t/(t+dT);
      ////
      gravity.x = alphat * gravity.x + (1 - alphat) * _accelerometerValues[0];
      gravity.y = alphat * gravity.y + (1 - alphat) * _accelerometerValues[1];
      gravity.z = alphat * gravity.z + (1 - alphat) * _accelerometerValues[2];
      ////
      linear_acceleration.x = _accelerometerValues[0] - gravity.x;
      linear_acceleration.y = _accelerometerValues[1] - gravity.y;
      linear_acceleration.z = _accelerometerValues[2] - gravity.z;
float[] grav={gravity.x,gravity.y,gravity.z};

      float I[] = new float[9];
      float O[] = new float[9];
      if (SensorManager.getRotationMatrix(O, I, grav, _magneticValues) )
        //      if (SensorManager.getRotationMatrix(R, I, _accelerometerValues, _magneticValues) )
      {

        
        //ajustement du repere
        SensorManager.remapCoordinateSystem(O, SensorManager.AXIS_X, SensorManager.AXIS_Y, R);
        
        
        ///------------------------------------------------------------procedure de calibration
        ///sur 1000 mesures, calcul de la moyenne du vecteur Ac
        //        if (calibration<=100){///1000 mesures
        //        correction.add(_accelerometerValues[0],_accelerometerValues[1],_accelerometerValues[2]);//cumul des mesures
        //        calibration++;        
        //        } else if (calibration<1500){///calcul de la moyenne et desactivation de la calibration
        //        correction.mult(-0.01);  //calcul de la moyenne des mesures
        //        correction.add(0,0,9.81);      
        //        calibration=1600;///desactivation de la calibration
        //        }

        //        float orientation[] = new float[3];
        //        orientation = SensorManager.getOrientation(R, orientation);
        //        
        //        _azimuth = (orientation[0]);
        //        _pitch = (orientation[1]);
        //        _roll = (orientation[2]);



        //calcul du deplacement
        //determination de l'acceleration lineaire

        // alpha is calculated as t / (t + dT)
        // with t, the low-pass filter's time-constant
        // and dT, the event delivery rate

        //      float alphat = 0.8;
        //
        //      gravity.x = alphat * gravity.x + (1 - alphat) * _accelerometerValues[0];
        //      gravity.y = alphat * gravity.y + (1 - alphat) * _accelerometerValues[1];
        //      gravity.z = alphat * gravity.z + (1 - alphat) * _accelerometerValues[2];
        //
        //      linear_acceleration.x = _accelerometerValues[0] - gravity.x;
        //      linear_acceleration.y = _accelerometerValues[1] - gravity.y;
        //      linear_acceleration.z = _accelerometerValues[2] - gravity.z;
        //
        //
        //
        //
        //      ///mise a jour de l'intervalle de temps ///pas utilise
        //      float dT=0;
        //temps passe depuis le dernier passage      





        ///----------------------------------------------------
        //determination de l'acceleration lineaire par soustraction de la gravite
        // alpha is calculated as t / (t + dT)
        // with t, the low-pass filter's time-constant
        // and dT, the event delivery rate
        //
        //float t=0.2;
        //       float alphat = t/(t+dT);
        ////
        //             gravity.x = alphat * gravity.x + (1 - alphat) * _accelerometerValues[0];
        //            gravity.y = alphat * gravity.y + (1 - alphat) * _accelerometerValues[1];
        //            gravity.z = alphat * gravity.z + (1 - alphat) * _accelerometerValues[2];
        ////
        //linear_acceleration.x = _accelerometerValues[0] - gravity.x;
        //             linear_acceleration.y = _accelerometerValues[1] - gravity.y;
        //             linear_acceleration.z = _accelerometerValues[2] - gravity.z;

        //             linear_acceleration.x = _accelerometerValues[0] - correction.x;
        //           linear_acceleration.y = _accelerometerValues[1] - correction.y;
        //            linear_acceleration.z = _accelerometerValues[2] - correction.z;


        // gravity.set(0, 0, R[8]*(-9.81)*dT*100);

    //    linear_acceleration.x =(R[0]*_accelerometerValues[0])+(R[1]*_accelerometerValues[1])+(R[1]*_accelerometerValues[2]);
    //    linear_acceleration.y =(R[3]*_accelerometerValues[0])+(R[4]*_accelerometerValues[1])+(R[5]*_accelerometerValues[2]);
    //    linear_acceleration.z =(R[6]*_accelerometerValues[0])+(R[6]*_accelerometerValues[1])+(R[8]*_accelerometerValues[2]);

             linear_acceleration.x =(R[0]*linear_acceleration.x)+(R[3]*linear_acceleration.y)+(R[6]*linear_acceleration.z);
            linear_acceleration.y =(R[2]*linear_acceleration.x)+(R[4]*linear_acceleration.y)+(R[7]*linear_acceleration.z);
            linear_acceleration.z =(R[2]*linear_acceleration.x)+(R[5]*linear_acceleration.y)+(R[8]*linear_acceleration.z);




      //  linear_acceleration.add(0, 0, -9.81);//enlevement de la gravite
        controle=linear_acceleration.z;
        linear_acceleration.mult(dT);




        float dt=0.5;
        //mise a vour de la vitesse  
        // linear_acceleration.mult(dt);      
        vitesse.add(linear_acceleration);
        //mise a jour du deplacement


        deplacement=vitesse;//ligne intermediaire pour satisfaire a la synthaxe PVector
        deplacement.mult(dT*50);

        //mise a jour de la position en fonction de la rotation
        //la matrice R a pour forme :
        /*
/  R[ 0]   R[ 1]   R[ 2]  \
         |  R[ 3]   R[ 4]   R[ 5]  |
         \  R[ 6]   R[ 7]   R[ 8]  /
         */




        ///avec la formule de multiplication de matrices
        ///calcul de la matrice de transformation globale

        //        float DX=(R[0]*deplacement.x)+(R[1]*deplacement.y)+(R[2]*deplacement.z);
        //           float DY=(R[3]*deplacement.x)+(R[4]*deplacement.y)+(R[5]*deplacement.z);
        //          float DZ=(R[6]*deplacement.x)+(R[7]*deplacement.y)+(R[8]*deplacement.z);
        //          
        //         position.x +=DX;
        //          position.y+=DY;
        //          position.z+=DZ;

        //     position.x +=(R[0]*deplacement.x)+(R[3]*deplacement.y)+(R[6]*deplacement.z);
        //    position.y +=(R[1]*deplacement.x)+(R[4]*deplacement.y)+(R[7]*deplacement.z);
        //    position.z +=(R[2]*deplacement.x)+(R[5]*deplacement.y)+(R[8]*deplacement.z);

        if (dessine) {

          //      position.x +=(R[0]*deplacement.x)+(R[1]*deplacement.x)+(R[2]*deplacement.x);
          //    position.y +=(R[3]*deplacement.y)+(R[4]*deplacement.y)+(R[5]*deplacement.y);
          //    position.z +=(R[6]*deplacement.z)+(R[7]*deplacement.z)+(R[8]*deplacement.z);

          position.x +=deplacement.x;
          position.y +=deplacement.y;
          position.z +=deplacement.z;

          //------------------------------------------------------------ memorisation dans le chemin

          if (curentStep< limitChemin) {    //mise a jour de la tete d'enregistrement
            curentStep++;
          }
          else {
            curentStep=1;
          }


          //creation ou mise a jour du mouvement
          if (curentStep< chemin.length-1) {   ///mise a jour
            chemin[curentStep].set(position);
          }
          else {//creation
            chemin=(PVector[]) expand(chemin, chemin.length+1);
            //chemin[chemin.length-1]=position;

            //chemin[curentStep].set(position);
            chemin[chemin.length-1]=new PVector(position.x, position.y, position.z);
          }
        }
      }//fin dessine
    }
  }




  void onAccuracyChanged(Sensor sensor, int accuracy) {
    // do nuthin'...
  }
}

